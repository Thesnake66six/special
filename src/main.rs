use crossterm::{
    cursor,
    event::{self, DisableMouseCapture, Event, KeyCode},
    execute,
    terminal::{
        self, disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen,
    },
};
use std::{
    io::{self, stdout, Write},
    thread, time,
};
use tuviv::{
    border::Border,
    le::{
        flexbox::{wrap, FlexLayout},
        layout::{Rect, Vec2},
        Orientation,
    },
    prelude::*,
    widgets::{Align, Flexbox, Paragraph},
    CrosstermBackend, Style, Widget,
};

fn main() -> io::Result<()> {
    let size = terminal::size()?;
    let slides = vec![
        Flexbox::new(Orientation::Vertical, false)
            .child(Paragraph::label("Happy".styled().yellow()).to_flex_child())
            .child(Paragraph::label("(belated)".styled().dimmed()).to_flex_child())
            .child(Paragraph::label("Birthday!".styled().yellow()).to_flex_child()),
        Flexbox::new(Orientation::Vertical, false)
            .child(Paragraph::label("Hope you have".styled().green()).to_flex_child())
            .child(Paragraph::label("a great year".styled().green()).to_flex_child())
            .child(Paragraph::label("to come!".styled().green()).to_flex_child()),
        Flexbox::new(Orientation::Vertical, false)
            .child(Paragraph::label("And thanks".styled().blue()).to_flex_child())
            .child(Paragraph::label("for being a".styled().blue()).to_flex_child())
            .child(Paragraph::label("great friend".styled().blue()).to_flex_child()),
        Flexbox::new(Orientation::Vertical, false)
            .child(Paragraph::label("This was meant".styled().blue()).to_flex_child())
            .child(Paragraph::label("to have a cool".styled().blue()).to_flex_child())
            .child(Paragraph::label("border effect".styled().blue()).to_flex_child()),
        Flexbox::new(Orientation::Vertical, false)
            .child(Paragraph::label("But I ran".styled().blue()).to_flex_child())
            .child(Paragraph::label("out of time".styled().blue()).to_flex_child())
            .child(Paragraph::label("so sadly no".styled().blue()).to_flex_child()),
    ];

    let mut stdout = stdout();
    execute!(stdout, EnterAlternateScreen, cursor::Hide)?;
    enable_raw_mode()?;
    let mut prevbuffer = tuviv::Buffer::new(Vec2::new(size.0.into(), size.1.into()));
    let mut slide = 0;
    let mut step = 0;

    loop {
        // Get the size of the terminal
        let size = terminal::size()?;

        // Create the buffer to write to
        let backend = CrosstermBackend;
        let mut buffer = tuviv::Buffer::new(Vec2::new(size.0.into(), size.1.into()));

        // Render the widget onto the buffer.
        let w = get_slide(slide).unwrap();
        cycle(w, step)
            .render(Rect::new(0, 0, size.0.into(), size.1.into()), &mut buffer);

        // Render the buffer to the terminal
        backend.finish(&buffer, &prevbuffer, &mut stdout)?;
        prevbuffer = buffer;
        if let Some(input) = get_input()? {
            let i = input;
            if i < 0 {
                break;
            }
            slide += 1;
            if slide == slides.len() {
                slide = 0
            }
        }
        // thread::sleep(time::Duration::from_millis(200));
        step += 1;
        if step == 6 {
            step = 0
        }
    }

    // Restore terminal
    disable_raw_mode()?;
    execute!(
        stdout,
        LeaveAlternateScreen,
        DisableMouseCapture,
        cursor::Show
    )?;

    Ok(())
}

fn cycle(widget: Box<Flexbox>, step: usize) -> Box<Align> {
    let STYLES: [tuviv::Style; 6] = [
        Style::default().red(),
        Style::default().yellow(),
        Style::default().green(),
        Style::default().cyan(),
        Style::default().blue(),
        Style::default().magenta(),
    ];

    widget
        .centered()
        .to_box_sizing()
        .border(Border::ROUNDED.styled(STYLES[step]))
        .fixed_size(15 + (4 * step), 7 + (2 * step))
        .centered()
}

fn get_input() -> Result<Option<i8>, io::Error> {
    if let Event::Key(key) = event::read()? {
        //Breaking on ESCAPE key
        Ok(Some(match key.code {
            KeyCode::Esc => -1,
            _ => 0,
        }))
    } else {
        Ok(None)
    }
}

fn get_slide(slide: usize) -> Option<std::boxed::Box<tuviv::widgets::Flexbox<'static>>> {
    match slide {
        0 => {
            return Some(Flexbox::new(Orientation::Vertical, false)
                .child(Paragraph::label("Happy".styled().yellow()).to_flex_child())
                .child(Paragraph::label("(belated)".styled().dimmed()).to_flex_child())
                .child(Paragraph::label("Birthday!".styled().yellow()).to_flex_child()));
        },

        1 => {
            return Some(Flexbox::new(Orientation::Vertical, false)
                .child(Paragraph::label("Hope you have".styled().green()).to_flex_child())
                .child(Paragraph::label("a great year".styled().green()).to_flex_child())
                .child(Paragraph::label("to come!".styled().green()).to_flex_child()));
        },

        2 => {
            return Some(Flexbox::new(Orientation::Vertical, false)
                .child(Paragraph::label("And thanks".styled().blue()).to_flex_child())
                .child(Paragraph::label("for being a".styled().blue()).to_flex_child())
                .child(Paragraph::label("great friend".styled().blue()).to_flex_child()));
        },

        _ => None,
    }
}